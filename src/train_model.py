# -*- coding: utf-8 -*-
"""Train model, serialize and save result."""

import pickle

import click
import pandas as pd

from src.constants import METRICS, RAW_DATA, RAW_TAGS, TRAINED_MODEL
from src.model import LogRegressor


@click.command()
@click.argument('data_path', type=click.Path(), default=RAW_DATA)
@click.argument('tags_path', type=click.Path(), default=RAW_TAGS)
@click.argument('model_path', type=click.Path(), default=TRAINED_MODEL)
@click.argument('metrics_path', type=click.Path(), default=METRICS)
def train(
    data_path: str, tags_path: str, model_path: str, metrics_path: str
) -> None:
    """Train model, serialize and save result.

    Args:
        data_path (str): Path to raw data
        tags_path (str): Path to data target (tags)
        model_path (str): Path for saving trained model
        metrics_path (str): Path for saving metrics
    """
    df = pd.read_csv(data_path, sep='\t', names=['question', 'tags'])
    print('\nRaw data:\n', df.head())

    df['tags'] = df['tags'].str.lower().str.strip().str.split()
    df['question'] = df['question'].str.lower().str.strip().str.split()
    print('\nPrepared dataset:\n', df.head())

    with open(tags_path) as f:
        top_tags = set(f.read().split('\n'))
    print('\nTags for training model:\n', top_tags)

    print('\nStart training:')
    model = LogRegressor(tags=top_tags)
    metrics = model.fit_dataframe(df)
    print('\nTraining completed! Metrics:\n', metrics.head())

    pickle.dump(model, open(model_path, 'wb'))
    print('\nModel saved:', model_path)

    metrics.to_csv(metrics_path)
    print('\nMetrics (loss) saved:', metrics_path)


if __name__ == '__main__':
    train()

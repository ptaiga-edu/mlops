# -*- coding: utf-8 -*-
"""Test model prediction on new sentence."""

import pickle

import click
import numpy as np
import pandas as pd

from src.constants import NEW_QUESTION, PREDICT_OUT, TRAINED_MODEL
from src.model import LogRegressor


def predict_proba(model: LogRegressor, sentence: str) -> dict:
    """Probabilities of tags for new sentence.

    Parameters
    ----------
    model: LogRegressor
    sentence: str

    Returns
    -------
    dict of tags with probabilities
    """
    p = {}
    for tag in model._tags:
        z = model._b[tag]
        for word in sentence.split(' '):
            if word not in model._vocab:
                continue
            z += model._w[tag][model._vocab[word]]
        sigma = 1 / (1 + np.exp(-z)) if z >= 0 else 1 - 1 / (1 + np.exp(z))
        p[tag] = sigma
    return p


@click.command()
@click.argument('model_path', type=click.Path(), default=TRAINED_MODEL)
@click.argument('topic_path', type=click.Path(), default=NEW_QUESTION)
@click.argument('predict_path', type=click.Path(), default=PREDICT_OUT)
@click.argument('threshold', type=float, default=0.7)
def predict(
    model_path: str, topic_path: str, predict_path: str, threshold: float
) -> list:
    """Load trained model, new topic and return top tags.

    Args:
        model_path (str): Path to trained model
        topic_path (str): Path to new topic
        predict_path (str): Path to save prediction
        threshold (float): Float number from 0 to 1

    Returns:
        List of tags with probabilities greater than given threshold
    """
    model = pickle.load(open(model_path, 'rb'))
    new = pd.read_csv(topic_path, sep='\t', names=['question', 'tags'])
    sentence = new['question'][0]
    print('\nNew question:\n', sentence)
    print('\nRelated tags:', new['tags'][0].split())

    pred = predict_proba(model, sentence.lower().replace(',', ''))
    tag_preds = sorted(pred.items(), key=lambda t: t[1], reverse=True)
    top_tags = list(filter(lambda t: t[1] > threshold, tag_preds))
    with open(predict_path, 'w') as f_out:
        f_out.write(str(top_tags))
    print(f'\nTags with probabilities greater than {threshold}:')
    print(top_tags)
    print('Saved:', predict_path)
    return top_tags


if __name__ == '__main__':
    # By default, click converts your program into a command-line program,
    # i.e. it executes the click command and exits with a return code.
    # You can change this behaviour by passing
    # the argument standalone_mode=False
    tags = predict(standalone_mode=False)
    if tags:
        print('\nPredicted tags:', [t[0] for t in tags])

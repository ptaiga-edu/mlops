# -*- coding: utf-8 -*-
"""Visualization module."""

import click
import matplotlib.pyplot as plt
import pandas as pd

from src.constants import METRICS, METRICS_IMG


@click.command()
@click.argument('metrics_path', type=click.Path(), default=METRICS)
@click.argument('image_path', type=click.Path(), default=METRICS_IMG)
@click.argument('show_plot', type=bool, default=False)
def visualize(metrics_path: str, image_path: str, show_plot: bool) -> None:
    """Plot metrics saved during trainig process.

    Args:
        metrics_path (str): Path to saved metrics
        image_path (str): Path for saving plot
        show_plot (bool): Show plot after saving
    """
    print('\nPlot data from:', metrics_path)
    metrics = pd.read_csv(metrics_path)
    plt.plot(pd.Series(metrics['loss'][:-10000]).rolling(10000).mean())
    plt.savefig(image_path)
    print('\nMetrics (loss) image saved:', image_path)
    if show_plot:
        plt.show()


if __name__ == '__main__':
    visualize()

# -*- coding: utf-8 -*-
"""LogRegressor class."""

from collections import defaultdict

import numpy as np
import pandas as pd


class LogRegressor:
    """LogRegressor class."""

    def __init__(self, tags: list) -> None:
        """LogRegressor class constructor.

        Parameters
        ----------
        tags: list of string
        """
        self.__version__ = 'v0.1'
        # `set` will drop duplicated tags
        self._tags = set(tags)

        # A dictionary that contains the mapping of sentence words and tags
        # into indexes (to save memory)
        # example: self._vocab ['exception'] = 17 means that the word
        # "exception" has an index of 17
        self._vocab = {}  # defaultdict(lambda: len(self._vocab))

        # parameters of the model: weights
        # for each class / tag we need to store its own vector of weights
        # By default, all weights will be zero
        # we do not know in advance how many scales we will need
        # so for each class we create a dictionary of a variable size with
        # a default value of 0
        # example: self._w['java'][self._vocab['exception']] contains
        # weight for word exception and tag java
        self._w = {t: defaultdict(int) for t in tags}

        # parameters of the model: bias term or w_0 weight
        self._b = {t: 0 for t in tags}

    def update_vocab(self, words_list: list) -> None:
        """Update vocab with new words from words_list.

        Parameters
        ----------
        words_list: list of strings
        """
        for word in words_list:
            # every new word will get index=len(self._vocab)
            # so at the end of training all wards will numbered
            # from 0 to len(self._vocab)
            if word not in self._vocab:
                self._vocab[word] = len(self._vocab)

    def generate_vocab(self, df: pd.DataFrame, column_name: str) -> None:
        """Build words vocab from dataframe column of lists.

        Parameters
        ----------
        df: pandas.Dataframe

        column_name: string
        """
        if column_name not in df.columns:
            raise ValueError("DataFrame doesnt have '{}' column!")
        df[column_name].map(self.update_vocab)

    def fit_sample(self, sample: pd.Series) -> pd.Series:
        """Fit single sample.

        Parameters
        ----------
        sample: pandas.Series
            dict-like object which contains qeustion and his tags

        Returns
        -------
        pandas.Series object with metrics for sample
        """
        # sample.name is value from df.index aka row number
        sample_id = sample.name
        question = sample['question']
        tags = set(sample['tags'])

        sample_loss = 0

        # derive the gradients for each tag
        for tag in self._tags:
            # target is 1 if current sample has current tag
            y = int(tag in tags)
            # calculate linear combination of weights and features
            z = self._b[tag]

            for word in question:
                is_word_unknown = word not in self._vocab
                # in the test mode, ignore the words that are not in
                # the vocabulary
                if sample_id >= self.top_n_train and is_word_unknown:
                    continue
                z += self._w[tag][self._vocab[word]]

            # calculate the probability of tag
            sigma = 1 / (1 + np.exp(-z)) if z >= 0 else 1 - 1 / (1 + np.exp(z))

            # update the value of the loss function for the current example
            sample_loss += (
                -np.log(np.max([self.tolerance, sigma]))
                if y
                else -np.log(1 - np.min([1 - self.tolerance, sigma]))
            )

            # If still in the training part, update the parameters
            if sample_id < self.top_n_train:
                # compute the log-likelihood derivative by weight
                dLdw = y - sigma

                # make gradient descent step
                # We minimize negative log-likelihood (second minus sign)
                # so we go to the opposite direction of the gradient
                # to minimize it (the first minus sign)
                delta = self.learning_rate * dLdw
                for word in question:
                    self._w[tag][self._vocab[word]] -= -delta
                self._b[tag] -= -delta
        if sample_id % self.show_period == 0:
            n = sample_id + self.show_period
            print(
                'LogRegressor {} | {} ({:.2f}%) samples fitted.'.format(
                    self.__version__, n, 100 * n / self.total_len
                )
            )
        return pd.Series({'loss': sample_loss})

    def fit_dataframe(
        self,
        df: pd.DataFrame,
        top_n_train: int = 43000,
        learning_rate: float = 0.1,
        tolerance: float = 1e-16,
    ) -> pd.DataFrame:
        """One run through dataframe.

        Parameters
        ----------
        df : pandas.DataFrame
            pandas DataFrame with question and tags data

        top_n_train : int
            first top_n_train samples will be used for training, the rest are
            for the test default=43000

        learning_rate : float
            gradient descent training speed
            default=0.1

        tolerance : float
            used for bounding the values of logarithm argument
            default=1e-16

        Returns
        -------
        pandas.DataFrame with metrics for each sample
        """
        self.total_len = df.shape[0]
        self.top_n_train = top_n_train
        self.learning_rate = learning_rate
        self.tolerance = tolerance

        if self.top_n_train > self.total_len:
            print(
                "Warning! 'top_n_train' more than dataframe rows count!n"
                "Set default 'top_n_train'=43000"
            )
            self.top_n_train = 43000

        # generating self._vocab
        self.generate_vocab(df, column_name='question')
        # Show progress every self.show_period sample, 1% by default
        self.show_period = self.total_len // 100
        # apply self.fit_sample to each row (sample) of dataframe
        self.metrics = df.apply(self.fit_sample, axis=1)
        return self.metrics

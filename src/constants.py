# -*- coding: utf-8 -*-
"""Constants module."""

from pathlib import Path

ROOT_DIR = Path(__file__).parent.parent

RAW_DATA = ROOT_DIR.joinpath('data', 'raw', 'stackoverflow_sample_50k.tsv.zip')
RAW_TAGS = ROOT_DIR.joinpath('data', 'raw', 'top10_tags.tsv')
NEW_QUESTION = ROOT_DIR.joinpath('data', 'external', 'new_question.tsv')
TRAINED_MODEL = ROOT_DIR.joinpath('models', 'model.pkl')
METRICS = ROOT_DIR.joinpath('models', 'metrics.csv')
METRICS_IMG = ROOT_DIR.joinpath('models', 'metrics_img.jpg')
PREDICT_OUT = ROOT_DIR.joinpath('models', 'predict_out.txt')

# -*- coding: utf-8 -*-
"""Experiments with MLFlow tracking.

To access the MLFLow tracking server, set
into environment (manual or via `.env`-file):
AWS_ACCESS_KEY_ID=...
AWS_SECRET_ACCESS_KEY=...
MLFLOW_S3_ENDPOINT_URL=...

Also `AWS_...` secrets can be put in `~/.aws/credential` file.
But `MLFLOW_S3_ENDPOINT_URL` set manual or via `.env`.

Load variable from `.env` to environment:
    from dotenv import load_dotenv
    load_dotenv()

Instead of custom model we will be using:
- `sklearn.linear_model.LogisticRegression` or
- `sklearn.ensemble.GradientBoostingClassifier`
for better understanding of all oportunities.

For simplicity begin with generative dataset:
    from sklearn.datasets import make_blobs
    from sklearn.model_selection import train_test_split
    X, y = make_blobs(n_samples=100, centers=3, n_features=5, random_state=1)
    X_train, y_train, X_test, y_test = train_test_split(X, y)

Continue with real data and multi-output classification using
`sklearn.multioutput.MultiOutputClassifier`.

"""

import numpy as np
import pandas as pd
from dotenv import load_dotenv
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.multioutput import MultiOutputClassifier
from sklearn.preprocessing import MultiLabelBinarizer

import mlflow
from mlflow.models.signature import infer_signature
from src.constants import NEW_QUESTION, RAW_DATA, RAW_TAGS

load_dotenv()


def main() -> None:
    """Tracking experiments with MLFlow."""
    mlflow.set_tracking_uri('http://192.168.1.54:5000')
    mlflow.set_experiment('sklearn')
    mlflow.sklearn.autolog()
    with mlflow.start_run():
        df = pd.read_csv(RAW_DATA, sep='\t', names=['question', 'tags'])
        df['tags'] = df['tags'].str.lower().str.strip().str.split()
        with open(RAW_TAGS) as f:
            top_tags = set(f.read().split('\n'))
        df['tags'] = df['tags'].apply(top_tags.intersection)
        print(df.head())

        vectorizer = TfidfVectorizer(stop_words='english')
        X = vectorizer.fit_transform(df['question'])

        mlb = MultiLabelBinarizer()
        y = mlb.fit_transform(df.tags)

        # `GradientBoostingClassifier` gives best results but it needs
        # too much time to train separate model for every output tag
        # via `MultiOutputClassifier`.
        lr = LogisticRegression(max_iter=200, verbose=1)
        model = MultiOutputClassifier(lr)
        model.fit(X, y)

        new = pd.read_csv(NEW_QUESTION, sep='\t', names=['question', 'tags'])
        new['tags'] = new['tags'].str.lower().str.strip().str.split()
        sentence = vectorizer.transform(new['question'])
        print(new['question'], new['tags'])

        pred = model.predict_proba(sentence)
        tags_prob = list(zip(mlb.classes_, [p[0][1] for p in pred]))
        y_pred = np.reshape([int(p[0][1] > 0.2) for p in pred], (1, -1))
        tags_pred = mlb.inverse_transform(y_pred)
        print(tags_prob, tags_pred)

        # Or simply use `autolog_run = mlflow.last_active_run()`  # noqa: SC100
        signature = infer_signature(sentence, np.array(pred))
        mlflow.sklearn.log_model(
            model, 'LogisticRegression',
            signature=signature,
            pyfunc_predict_fn='predict_proba'
        )


if __name__ == '__main__':
    main()

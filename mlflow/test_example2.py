# -*- coding: utf-8 -*-
"""MLFlow usage. Example #2.

Tracking of parameters, metrics, artifats.
"""

import os
from random import randint, random

import mlflow
from mlflow import log_artifacts, log_metric, log_param


def main() -> None:
    """Perform base operation."""
    mlflow.set_tracking_uri('http://127.0.0.1:5000')
    mlflow.set_experiment('mlflow_test')
    log_param('param1', randint(0, 100))

    log_metric('foo', random())
    log_metric('foo', random() + 1)
    log_metric('foo', random() + 2)

    if not os.path.exists('outputs'):
        os.makedirs('outputs')
    with open('outputs/test.txt', 'w') as f:
        f.write('Hello, world!')
    log_artifacts('outputs')


if __name__ == '__main__':
    main()

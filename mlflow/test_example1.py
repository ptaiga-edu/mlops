# -*- coding: utf-8 -*-
"""MLFlow usage. Example #1.

Tracking of sklearn's models.

Local MLFlow server with local files:
`$ mlflow ui`

Local MLFlow server with SQLite datbase:
`$ mlflow server --backend-store-uri sqlite:///mlflow.db --default-a
rtifact-root ./mlruns/artifacts/ --host 127.0.0.1`
"""

from sklearn.datasets import load_diabetes
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split

import mlflow
from mlflow.models.signature import infer_signature


def main() -> None:
    """Perform base operation."""
    mlflow.set_tracking_uri('http://127.0.0.1:5000')
    mlflow.set_experiment('sklearn')
    mlflow.sklearn.autolog()
    with mlflow.start_run():
        data = load_diabetes()
        X_train, X_test, y_train, y_test = train_test_split(
            data.data, data.target
        )
        rf = RandomForestRegressor(
            n_estimators=100, max_depth=6, max_features=3
        )
        rf.fit(X_train, y_train)
        y_pred = rf.predict(X_test)

        signature = infer_signature(X_train, y_pred)
        mlflow.sklearn.log_model(rf, 'diabetes_rf', signature=signature)
        # Or use autolog_run = mlflow.last_active_run()  # noqa: SC100


if __name__ == '__main__':
    main()

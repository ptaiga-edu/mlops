# Docker

## Server side

Install Docker (with Docker Compose) and Portainer on server side.

Copy `docker-compose.yaml`, `.env`, `nginx.conf` and `mlflow_image/Dockerfile` to the working directory on the server (e.g., `/root/docker/`).

For building inference server to serve trained models copy related data from https://gitlab.com/ptaiga-edu/inference

Run `$ docker compose up --build` for the first time and this will create containers with:
 - postgres
 - pgadmin
 - minio (+ nginx)
 - mlflow
 - inference

As option: if comment/uncomment lines related to `nginx` then will down/up container with proxy to MinIO.

Open in browser using `host:port` to try connection. Inside private network, e.g.:
 - Portainer https://192.168.1.54:9443
 - MinIO http://192.168.1.54:9001
 - PGAdmin http://192.168.1.54:5050
 - MLFlow http://192.168.1.54:5000
 - Inference API http://192.168.1.54:8001/invocations


## Client side

For correct working of MLFlow tracking perform the one of next recommendations.

### Manual

Copy `credentials` file into ~/.aws or set this variables into environment:
 - `AWS_ACCESS_KEY_ID=...`
 - `AWS_SECRET_ACCESS_KEY=...`

Set this into environment:
 - `MLFLOW_S3_ENDPOINT_URL=...`

### .env

Install `python-dotenv` into virtual environment.

Create `.env` file with (or add to existing file):
`AWS_ACCESS_KEY_ID=...
AWS_SECRET_ACCESS_KEY=...
MLFLOW_S3_ENDPOINT_URL=...`

Inside `.py` script use:
`from dotenv import load_dotenv
load_dotenv`

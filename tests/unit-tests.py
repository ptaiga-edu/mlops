# -*- coding: utf-8 -*-
"""Using unittest library to test algorithm."""

import unittest


class TestSimple(unittest.TestCase):
    """Simple tests."""

    def test_simple_cases(self):
        """Test several simple cases."""
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()

# Knowledge base


## Description

Project for MLOps 2.0 course (spring 2023)

On the 1st stage of the project we are trying to predict probabilities of tags for giving sentence. We will use dataset of tagged Stackoverflow questions for training and testing our model. Model is self-writing logistic regression class.


## Scripts

`$ python src/constans.py` - defualt settings.

`$ python src/train_model.py` - train model by default or
`$ python src/train_model.py [DATA_PATH] [TAGS_PATH] [MODEL_PATH] [METRICS_PATH]`. Use `--help` option for more information.

`$ python src/predict_model.py` - test prediction by default sentence or
`$ python src/predict_model.py [MODEL_PATH] [TOPIC_PATH] [PREDICT_PATH] [THRESHOLD]`

`$ python src/visualize.py` - plot training metrics and save as jpg or
`$ python src/visualize.py [METRICS_PATH] [IMAGE_PATH] [SHOW_PLOT]`

`$ python main.py` - perform all stages (train, predict, visualise) with default settings or use `$ dvc repro` to launch `dvc.yaml` pipeline with custom parameters.


## Environment

### PIP (Linux way)
Use next command to set required packages:
- `$ python3.8 -m venv mlops`
- `$ source mlops/bin/activate`
- `$ pip install -r requirements.txt`

### CONDA (Windows way)
Create environment:
- `$ conda create -n mlops python=3.8`
- `$ conda activate mlops`

In file `requirements.txt` comment on marked lines and perform:
- `$ conda install --file requirements.txt`

For much fuster installation of DVC packages using CONDA
comment lines `dvc` and `dvc-s3`, perform previous command
and later use:
- `$ conda install -c conda-forge mamba`
- `$ mamba install -c conda-forge dvc dvc-s3`

Install local packages:
- `$ pip install -e .`


## Docker

For more information see [`docker/`](docker/) folder and [`docker/README.md`](docker/README.md) in it.


## MLFlow

Tracking models and artifacts scripts placed in [`mlflow/`](mlflow/) folder.

Training model for inference perfomed in [`mlflow/track_model.py`](mlflow/track_model.py) scripts.


## Inference

For more information see https://gitlab.com/ptaiga-edu/inference project.


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>

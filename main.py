# -*- coding: utf-8 -*-
"""Main module."""

from src.constants import (
    METRICS,
    METRICS_IMG,
    NEW_QUESTION,
    RAW_DATA,
    RAW_TAGS,
    TRAINED_MODEL,
)
from src.predict_model import predict
from src.train_model import train
from src.visualize import visualize


def main() -> None:
    """Execute `train`, `predict` and `visualize` by default."""
    print('Execute `train`, `predict` and `visualize` by default.')
    print('Path to raw data:', RAW_DATA)
    print('Path to target (tags):', RAW_TAGS)
    print('Path for saving model:', TRAINED_MODEL)
    print('Path to metrics:', METRICS)
    print('Path for saving plot:', METRICS_IMG)
    print('Path to new topic:', NEW_QUESTION)
    print('Threshold for predicted tags:', 0.7)
    print('Show plot after saving:', False)
    train(standalone_mode=False)
    predict(standalone_mode=False)
    visualize(standalone_mode=False)


if __name__ == '__main__':
    main()
